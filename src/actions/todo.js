import { createAction } from "redux-actions";

import { types } from "../types/todo";

// const addTask = task => ({
//   type: types.TODO_ADD_TASK,
//   payload: task,
// })
// const actions = {
//   addTask,
// }

const actions = {
  // Criando uma ação pelo createAction. Ela recebe o type e retorna outra função
  // que é o PAYLOAD - exemplo: dispatch(actions.addTask(task));
  // Precisamos importar nos reducers uma outra função chamada handleActions.
  addTask: createAction(types.TODO_ADD_TASK),
  loading: createAction(types.TODO_LOADING),
  ageUp: createAction(types.TODO_AGE_UP),
  ageDown: createAction(types.TODO_AGE_DOWN),
  getPageList: createAction(types.GET_PAGE_LIST),
};

export { actions };
