import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import { reducers } from "./reducers";

// const reducer = (state = [], action) => {
//   switch (action.type) {
//     case "ADD_TASK":
//       return [...state, action.payload];

//     default:
//       return state;
//   }
// };

const middlewares = [logger];

// const store = createStore(
//   reducers,
//   applyMiddleware(...middlewares,)
// )
const store = createStore(reducers, applyMiddleware(...middlewares));

export { store };
